#Boa:Frame:mygrsync
# -*- coding: utf-8 -*-
import sys
import os
import subprocess
import time
import locale
import winsound
import wx
import wx.stc

import config

class G:
    str_encode = locale.getdefaultlocale()[1]
    _pg_RSYNC = 'rsync'
    _pg_GUIDIFF = 'bc2'

    if sys.platform == 'win32':
        if sys.version >= '2.6':
            nn = subprocess._subprocess
        else :
            nn = subprocess
        startupinfo = subprocess.STARTUPINFO()
        startupinfo.dwFlags |= nn.STARTF_USESHOWWINDOW
        startupinfo.wShowWindow = nn.SW_HIDE
        if sys.version >= '2.7':
            # win32 py>=2.7 subprocess._subprocess.CREATE_NEW_PROCESS_GROUP 512
            creationflags = subprocess._subprocess.CREATE_NEW_PROCESS_GROUP
        else:
            creationflags = 0
    else:
        startupinfo = None
        creationflags = 0

def create(parent):
    return mygrsync(parent)

[wxID_MYGRSYNC, wxID_MYGRSYNCBTNEXIT, wxID_MYGRSYNCBTNGETDIRS, 
 wxID_MYGRSYNCBTNSHOWLOG, wxID_MYGRSYNCCHK_TOPWINDOW, wxID_MYGRSYNCPANEL1, 
] = [wx.NewId() for _init_ctrls in range(6)]

class mygrsync(wx.Frame):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Frame.__init__(self, id=wxID_MYGRSYNC, name=u'mygrsync', parent=prnt,
              pos=wx.Point(580, 0), size=wx.Size(231, 93),
              style=wx.DEFAULT_FRAME_STYLE, title=u'mygrsync')
        self.SetClientSize(wx.Size(215, 55))
        self.Bind(wx.EVT_CLOSE, self.OnMygrsyncClose)

        self.panel1 = wx.Panel(id=wxID_MYGRSYNCPANEL1, name='panel1',
              parent=self, pos=wx.Point(0, 0), size=wx.Size(215, 55),
              style=wx.TAB_TRAVERSAL)
        self.panel1.Bind(wx.EVT_LEFT_DCLICK, self.OnPanel1LeftDclick)
        self.panel1.Bind(wx.EVT_LEFT_UP, self.OnPanel1LeftUp)
        self.panel1.Bind(wx.EVT_LEFT_DOWN, self.OnPanel1LeftDown)

        self.btnGetDirs = wx.Button(id=wxID_MYGRSYNCBTNGETDIRS,
              label=u'Look Copyed ', name=u'btnGetDirs', parent=self.panel1,
              pos=wx.Point(8, 8), size=wx.Size(88, 40), style=0)
        self.btnGetDirs.Bind(wx.EVT_BUTTON, self.OnBtnGetDirsButton,
              id=wxID_MYGRSYNCBTNGETDIRS)

        self.btnExit = wx.Button(id=wxID_MYGRSYNCBTNEXIT, label=u'EXIT',
              name=u'btnExit', parent=self.panel1, pos=wx.Point(104, 8),
              size=wx.Size(40, 40), style=0)
        self.btnExit.SetForegroundColour(wx.Colour(255, 0, 0))
        self.btnExit.Bind(wx.EVT_BUTTON, self.OnBtnExitButton,
              id=wxID_MYGRSYNCBTNEXIT)

        self.chk_topwindow = wx.CheckBox(id=wxID_MYGRSYNCCHK_TOPWINDOW,
              label=u'Top', name=u'chk_topwindow', parent=self.panel1,
              pos=wx.Point(160, 0), size=wx.Size(48, 24), style=0)
        self.chk_topwindow.SetValue(False)
        self.chk_topwindow.Bind(wx.EVT_CHECKBOX, self.OnChk_topwindowCheckbox,
              id=wxID_MYGRSYNCCHK_TOPWINDOW)

        self.btnShowLog = wx.Button(id=wxID_MYGRSYNCBTNSHOWLOG, label=u'>>',
              name=u'btnShowLog', parent=self.panel1, pos=wx.Point(160, 24),
              size=wx.Size(48, 24), style=0)
        self.btnShowLog.Bind(wx.EVT_BUTTON, self.OnBtnShowLogButton,
              id=wxID_MYGRSYNCBTNSHOWLOG)

    def __init__(self, parent):
        self._init_ctrls(parent)
        di = os.path.dirname(os.path.abspath(__file__))
        if os.path.isfile(di):
            di = os.path.dirname(di)
        if os.path.isdir(di):
            os.chdir(di)

        self.initctls()

    def initctls(self):
        self.leftmd = False
        ff = wx.Font(9, wx.SWISS, wx.NORMAL, wx.NORMAL, False, u'Courier New')
        self.panel1.Bind(wx.EVT_MOTION, self.OnPanelMotion)
        self.settopwindow()
        self.initlogwin(ff)

    def initlogwin(self, ff):
        dc = wx.ScreenDC()
        sz = dc.GetSize()
        dc.Destroy()
        self._logdlg = wx.Dialog(self, title=u"mygrsync log", size=wx.Size(440,640),
                                 pos = (sz.x-440, 0),
                                 style=wx.DEFAULT_DIALOG_STYLE | wx.RESIZE_BORDER)
        self._logstc = wx.stc.StyledTextCtrl(self._logdlg)
        self._logstc.StyleSetFont(wx.stc.STC_STYLE_DEFAULT, ff)
        self._logstc.SetMarginType(1, wx.stc.STC_MARGIN_NUMBER)
        self._logstc.SetMarginWidth(1, 24)
        self._logstc.SetSize(self._logdlg.GetClientSize())
        self._logstc.AppendText('%s\n%s\n\n' % (
            time.strftime('  %Y-%m-%d %H:%M:%S\n'), os.getcwdu()))
        self._logstc.EmptyUndoBuffer()
        if '--stdout' not in sys.argv:
            self._stdout, self._stderr = sys.stdout, sys.stderr
            sys.stdout, sys.stderr = self, self

    def OnMygrsyncClose(self, event):
        event.Skip()
        if hasattr(self, '_stdout'):
            sys.stdout, sys.stderr = self._stdout, self._stderr
            print 'reset sys.stdout, mainframe close'

    def write(self, txt):
        if type(txt) == type(u''):
            pass
        elif type(txt) == type(''):
            txt = txt.decode(G.str_encode, 'replace')
        else:
            txt = str(txt).decode(G.str_encode, 'replace')
        self._logstc.AppendText(txt)
        self._logstc.GotoLine(self._logstc.GetLineCount())

    def OnChk_topwindowCheckbox(self, event):
        self.settopwindow()

    def OnPanel1LeftDclick(self, event):
        self.settopwindow()

    def settopwindow(self):
        if self.GetWindowStyle() != wx.STAY_ON_TOP:
            if not hasattr(self, '_winstyle'):
                self._winstyle = self.GetWindowStyle()
            self.SetWindowStyle(wx.STAY_ON_TOP)
            if sys.platform == 'win32':
                sz = self.GetSize()
                self.SetSize(wx.Size(sz.x, sz.y - self.btnExit.GetSize().y))
            self.chk_topwindow.SetValue(True)
        else:
            self.SetWindowStyle(self._winstyle)
            if sys.platform == 'win32':
                sz = self.GetSize()
                self.SetSize(wx.Size(sz.x, sz.y + self.btnExit.GetSize().y))
            self.chk_topwindow.SetValue(False)

    def OnPanel1LeftDown(self, event):
        event.Skip()
        self.panel1.CaptureMouse()
        mouse=wx.GetMousePosition()
        frame=self.GetPosition()
        self.delta=wx.Point(mouse.x-frame.x,mouse.y-frame.y)
        self.leftmd = True

    def OnPanel1LeftUp(self, event):
        event.Skip()
        if self.panel1.HasCapture():
            self.panel1.ReleaseMouse()
        self.leftmd = False

    def OnPanelMotion(self, event):
        if event.Dragging() and self.leftmd:
            mouse=wx.GetMousePosition()
            self.Move((mouse.x-self.delta.x,\
                       mouse.y-self.delta.y))

    def OnBtnShowLogButton(self, event):
        event.Skip()
        if self._logdlg.IsShown():
            self._logdlg.Hide()
        else:
            self._logdlg.Show()
        subprocess.Popen('"%s" "%s"' % (config.editor, config.filelog))

    def OnBtnExitButton(self, event):
        event.Skip()
        self.Close()

    def OnBtnGetDirsButton(self, event):
        print 'begin'
        
        jobs = {}
        clip = wx.TheClipboard
        if clip.Open():
            cf = wx.FileDataObject()
            fs = []
            if clip.GetData(cf):
                fs = cf.GetFilenames()
            clip.Close()
            for f in fs:
                dst = os.path.dirname(f)
                if dst[-1] != '\\': dst += '\\'
                if dst[0].upper() in config.dst:
                    dst = config.dst[dst[0].upper()] + dst[1:]
                else:
                    winsound.Beep(3000, 160)
                if dst in jobs:
                    jobs[dst].append(f)
                else:
                    jobs[dst] = [f]
        for dst, srcs in jobs.items():
            print '--------\n%s<----\n%s\n' % (dst, '\n'.join(srcs))
            srcs = ' '.join('"%s"' % f for f in srcs)
            cmds = (
                '''"%s"'''
                ''' /cmd=diff /auto_close /open_window'''
                ''' /no_exec /error_stop /bufsize=32'''
                ''' /log /filelog=filelog.log /utf8'''
                ''' /disk_mode=auto /speed=autoslow'''
                '''  %s /to="%s" ''' % (config.fastcopy, srcs, dst) )
            print cmds
            subprocess.Popen(cmds)
        print 'end'

